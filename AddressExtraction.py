import re
import pandas as pd
from six.moves import xrange
import psycopg2
import nltk
import roman

jalan_terms=['jalan','jl','jln']
gedung_terms=['gedung','gd','apartemen','apt','perumahan','menara','wisma','kompleks','perum','komplek','villa']
perusahaan_terms=['pt']
nomor_terms=['blok','no','nomer','nomor','kav','unit','lantai','lt']
kelurahan_terms=['kel','Kelurahan','desa','Ds']
kecamatan_terms=['kec','Kecamatan']
rt_terms=['rt','rt/rw']
rw_terms=['rw']
kota_terms=['kota','kab','kabupaten']
prov_terms=['prov','propinsi','prop','provinsi']

def ngram(s):
    subs=s.split(" ")
    ngrams=[]
    for i in range(1,len(subs)+1):
        ngrams.append(' '.join(subs[0:i]))
    return ngrams

def read_file(file,idx):
    l=[]
    df = pd.read_csv(file)
    for d in df.itertuples():
        l.append(d[idx].lower())
    return l

list_kel=read_file("D:/data-bhinneka/AREA/AREA/bc_subdistrict.csv",3)
list_kec=read_file("D:/data-bhinneka/AREA/AREA/bc_district.csv",3)
list_kab=read_file("D:/data-bhinneka/AREA/AREA/bc_city.csv",3)
list_prop=read_file("D:/data-bhinneka/AREA/AREA/bc_province.csv",2)

def cleanse(s):
    cleaned=s.lower().replace(",", "").replace(".", " ")
    result=re.sub(' +',' ',cleaned)
    return result

def removestring(s,sub):
    if sub:
        temp=s.replace(sub,"",1)
        result=' '.join(temp.split())
        return result
    else:
        return s

def load_file():
    file = 'kodepos.xlsx'
    worksheet = 'Kodepos'

    data = pd.read_excel(file, sheet_name=worksheet)
    kodepos_dict = {}

    for d in data.itertuples():
        zip = str(d._1)
        kelurahan = d.Kelurahan
        kecamatan = d.Kecamatan
        kabupaten = d.Kabupaten
        provinsi = d.Propinsi
        value = [kelurahan, kecamatan, kabupaten, provinsi]
        isexist = kodepos_dict.get(zip, '0')
        kodepos_dict.setdefault(zip, [])
        if isexist != 0:
            kodepos_dict[zip].append(value)
        else:
            kodepos_dict[zip] = value
    return kodepos_dict

def search_terms(terms, address):
    result=[]
    tokens=nltk.word_tokenize(address)
    for t in tokens:
        for term in terms:
            if t==term:
                result.append(t)
    return result

format_1=['address','gedung','jalan','nomor','rt','rw','kode pos','kelurahan','kecamatan','kabupaten','provinsi']
format_2=['desa','rt','rw','kecamatan','kabupaten']
format_3=['jalan','nomor','rt','rw']
fieldsinput=['kelurahan','kecamatan','jenis','kabupaten','provinsi']

def extract_kodepos(address):
    address=cleanse(address)
    reg = re.compile('\d{5}')
    kodepos=reg.findall(address)
    found=False
    zipcode=""
    if len(kodepos)!=0:
        address=removestring(address, kodepos[0])
        found=True
        zipcode=kodepos[0]
    print("Kode Pos: "+zipcode)
    return zipcode,address,found

def get_mapping(mapping, address):
    for m in mapping:
        kel=m[0]
        if kel.lower() in address:
            return m
    return mapping[0]


def search_string(address, substr):
    try:
        found=re.search(substr.lower(), address).group(0)
    except:
        found=None
    return found

def get_info(m, format, address):
    kelurahan = m[0]
    kecamatan = m[1]
    kabupaten = m[2]
    provinsi = m[3]
    foundkel = search_string(address, kelurahan)
    print("Kelurahan: "+kelurahan)
    address=removestring(address,foundkel)
    foundkec = search_string(address, kecamatan)
    print("Kecamatan: "+kecamatan)
    address=removestring(address,foundkec)
    foundkab = search_string(address, kabupaten)
    print("Kabupaten/Kota: "+kabupaten)
    address=removestring(address,foundkab)
    foundprov = search_string(address, provinsi)
    print("Provinsi: "+provinsi)
    address=removestring(address,foundprov)
    return kelurahan,kecamatan,kabupaten,provinsi,address

output=[]

def extract_gedung(address, gedung_terms):
    term = search_terms(gedung_terms, address)
    gedung=""
    if len(term)>0 and address:
        next = address.split(term[0], 1)[1]
        next_term = search_terms(jalan_terms, next)
        try:
            gedung = term[0] + " " + next.split(next_term[0], 1)[0]
        except:
            pass
        gedung = re.sub(' +', ' ', gedung)
        address = removestring(address, gedung)
    print('Gedung/Apartemen: '+gedung)
    return gedung,address

def splitstring(address, substr):
    result=[]
    tokens=nltk.word_tokenize(address)
    for i in range(len(tokens)):
        if tokens[i]==substr:
            result.append(' '.join(tokens[:i]))
            result.append(' '.join(tokens[i:]))
    return result

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def extract_street(address,jalan_terms):
    term = search_terms(jalan_terms, address)
    jln=""
    if len(term)>0 and address:
        next = address.split(term[0], 1)[1]
        next_term = search_terms(nomor_terms, next)
        next_term_rt=search_terms(rt_terms, next)
        if len(next_term)>0:
            n=next_term[0]
            jln = splitstring(address,n)[0]
        elif hasNumbers(next):
            reg = re.compile(r'\w*\d\w*')
            n = reg.findall(address)[0]
            jln = term[0] + " " + next.split(n, 1)[0]
        else:
            jln=address
        jln=re.sub(' +',' ',jln)
        address = removestring(address, jln)
    print("Jalan: "+jln)
    return jln,address

_digits = re.compile('\d')
def contains_digits(d):
    return bool(_digits.search(d))

def search_numbers(s):
    result=[]
    tokens=nltk.word_tokenize(s)
    for t in tokens:
        if contains_digits(t):
            result.append(t)
    return result

def extract_number(address,no_terms):
    if address:
        term = search_terms(no_terms, address)
        if len(term)==0:
            term=""
        nmr = search_numbers(address)
        result=""
        if len(term)==len(nmr):
            for t,n in zip(term,nmr):
                result+= t + " " + n + " "
        elif len(nmr)==0 and len(term)>0:
            try:
                nomor = roman.fromRoman(get_token(term[0], address).upper())
                result=term[0]+" "+str(nomor)
            except:
                result=""
        else:
            result=nmr[0]
        result=re.sub(' +',' ',result)
        address = removestring(address, result)
        print("Nomor bangunan: "+result)
        return result,address

def get_token(d, string):
    result=""
    tokens=nltk.word_tokenize(string)
    for i in range(len(tokens)):
        if d==tokens[i]:
            result=tokens[i+1]
        elif d in tokens[i]:
            result=tokens[i].split(d)[1]
    return result

def extract_rtrw(address, rt, rw):
    if address:
        term=search_terms(rt_terms, address)
        reg = re.compile(r'\w*\d\w*')
        nomor = reg.findall(address)
        term_rw=search_terms(rw_terms, address)
        rt =""
        rw =""
        if len(term)>0:
            t=term[0]
            next=address.split(t)[1]
            if len(nomor)!=0 and '/' in next and len(term_rw)==0:
                rt=t+" "+nomor[0]
                reg = re.compile(r'\w*\d\w*')
                nomor = reg.findall(address)
                nomor_rw=""
                if len(nomor)<2:
                    nomor_rw=roman.fromRoman(get_token("/",address).upper())
                else:
                    nomor_rw=nomor[1]
                rw="rw "+str(nomor_rw)
                address = removestring(address, term[0]+" "+nomor[0])
            elif len(nomor)!=0 and address:
                rt=t+" "+nomor[0]
                address = removestring(address, rt)
                term = search_terms(rw_terms, address)
                if len(term)>0:
                    reg = re.compile(r'\w*\d\w*')
                    nomor = reg.findall(address)
                    if len(nomor)>0:
                        rw=term[0]+" "+nomor[0]
                address = removestring(address, rw)
            print("RT/RW: " + rt + "/" + rw)
        return rt,rw,address

def extract_kel(s,kel_terms):
    if s:
        term = search_terms(kel_terms, s)
        result=""
        t=""
        if len(term)>0:
            t=term[0]
            next = s.split(term[0], 1)[1]
            next=next.lstrip()
        else:
            next=s
        flag=0
        ngrams=ngram(next)
        for l in list_kel:
            for i in reversed(xrange(len(ngrams))):
                if ngrams[i].lower() == l and flag!=1:
                    result = ngrams[i]
                    flag=1
        kel=(t+" "+result).lstrip()
        address = removestring(s, kel)
        print("Kelurahan: " + kel)
        return kel,address

def extract_kec(s,kec_terms):
    if s:
        term = search_terms(kec_terms, s)
        result=""
        t=""
        if len(term)>0:
            t=term[0]
            next = s.split(term[0], 1)[1]
            next=next.lstrip()
        else:
            next=s
        flag = 0
        ngrams = ngram(next)
        for l in list_kec:
            for i in reversed(xrange(len(ngrams))):
                if ngrams[i].lower() == l and flag != 1:
                    result = ngrams[i]
                    flag = 1
        kec=(t+" "+result).lstrip()
        address = removestring(s, kec)
        print("Kecamatan: " + kec)
        return kec,address

def extract_city(s,kab_terms):
    if s:
        term = search_terms(kab_terms, s)
        result=""
        t=""
        if len(term)>0:
            t=term[0]
            next = s.split(term[0], 1)[1]
            next=next.lstrip()
        else:
            next=s
        flag = 0
        ngrams = ngram(next)
        for l in list_kab:
            for i in reversed(xrange(len(ngrams))):
                if ngrams[i].lower() == l and flag != 1:
                    result = ngrams[i]
                    flag = 1
        kab=(t+" "+result).lstrip()
        address = removestring(s, kab)
        print("Kota/Kab: " + kab)
        return kab,address

def extract_prov(s):
    if s:
        term = search_terms(prov_terms, s)
        result=""
        t=""
        if len(term)>0:
            t=term[0]
            next = s.split(term[0], 1)[1]
            next=next.lstrip()
        else:
            next=s
        flag = 0
        ngrams = ngram(next)
        for l in list_prop:
            for i in reversed(xrange(len(ngrams))):
                if ngrams[i].lower() == l and flag != 1:
                    result = ngrams[i]
                    flag = 1
        prov=(t+" "+result).lstrip()
        address = removestring(s, prov)
        print("Provinsi: " + prov)
        return prov,address

allowed_chars=['/','&','(',')',':','.','-','\'',',',' ']

def check_characters(str):
    found=False
    flag=0
    for s in str:
        if s not in allowed_chars and not s.isalnum():
            flag=1
    if flag:
        found=True
    return found

def formatting_address(address):
    flag="0"
    address_2=None
    if not address:
        flag="50"
    else:
        addr=address.replace("\n"," ")
        address = re.sub(' +', ' ', addr)
        if check_characters(address):
            flag="51"
        length=len(address)
        if length<5:
            flag="52"
        if length>250:
            address_2=address[250:]
    result=address
    return result,flag,address_2

conn_string = "host='b2c-api-dev.chzl1mgg7vpx.ap-southeast-1.rds.amazonaws.com' dbname='user_service' user='b2c_api' password='B2C4P1DEVP455W0RD'"
conn = psycopg2.connect(conn_string)
cur = conn.cursor()

q='select address from public.member order by "lastLogin" desc limit 5000'
cur.execute(q)
rows=cur.fetchall()

def select_functions(i,address):
    result=""
    address=""
    if i==1:
        result,address=extract_gedung(address,gedung_terms)
    if i==2:
        result,address=extract_street(address,jalan_terms)
    if i==3:
        result,address=extract_number(address,nomor_terms)
    if i==4:
        result,address=extract_rtrw(address,rt_terms,rw_terms)
    return result,address

#rows=['Jl. Mega Raya no. II']

for row in rows:
    add,flag,ad2=formatting_address(row[0])
    kodepos=""
    gedung=""
    jalan=""
    nomor=""
    rt=""
    rw=""
    kelurahan=""
    kecamatan=""
    kabupaten=""
    provinsi=""
    if flag=="0":
        address=add.lower()
        kodepos,address,found=extract_kodepos(address)
        gedung,address=extract_gedung(address,gedung_terms)
        jalan,address=extract_street(address,jalan_terms)
        if address:
            nomor,address=extract_number(address,nomor_terms)
            if address:
                rt,rw,address=extract_rtrw(address,rt_terms,rw_terms)
            if not found and address:
                kelurahan,address=extract_kel(address,kelurahan_terms)
                if address:
                    kecamatan,address=extract_kec(address,kecamatan_terms)
                if address:
                    kabupaten,address=extract_city(address,kota_terms)
                if address:
                    provinsi,address=extract_prov(address)
            elif address:
                kodepos_dict = load_file()
                mapping = kodepos_dict.get(kodepos, 'Invalid')
                if mapping!='Invalid':
                    m = get_mapping(mapping, address)
                    result=get_info(m, 1, address)
                    kelurahan=result[0]
                    kecamatan=result[1]
                    kabupaten=result[2]
                    provinsi=result[3]
    output.append([row[0],gedung,jalan,nomor,rt,rw,kodepos,kelurahan,kecamatan,kabupaten,provinsi])
outputdf = pd.DataFrame(output, columns=format_1)
outputdf.to_csv("D:/data-bhinneka/extraction.csv")