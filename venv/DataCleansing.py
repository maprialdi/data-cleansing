import re
from datetime import datetime
import itertools
import pandas as pd
import psycopg2

#fungsi untuk validitas email menggunakan format standar email

def load_data(file):
    dict={}
    df = pd.read_csv(file)
    for d in df.itertuples():
        dict[d[2]] = d[3]
    return dict

dict_province=load_data("D:/data-bhinneka/AREA/AREA/bc_province.csv")
dict_city=load_data("D:/data-bhinneka/AREA/AREA/bc_city.csv")
dict_district=load_data("D:/data-bhinneka/AREA/AREA/bc_district.csv")
dict_subdistrict=load_data("D:/data-bhinneka/AREA/AREA/bc_subdistrict.csv")

error_message={'10':'Gender is empty','11':'Gender is invalid','20':'Email is empty','21':'Email format is invalid','22':'Email is not available',
               '30':'Phone number is empty','31':'Phone number should be numeric only','32':'Phone number length is invalid',
               '33':'Phone number should be started with 8 and not with 84 or 86','40':'Birthdate is empty','41':'Unknown birthdate format',
               '50':'Address is empty','51':'Address contains invalid characters','52':'Invalid address length'}
allowed_chars=['/','&','(',')',':','.','-','\'',',',' ']

def check_unique(cur,email):
    sql="SELECT email from public.member where email='%s'"
    cur.execute(sql,email)
    rows=cur.fetchall()
    if len(rows)>0:
        return False
    else:
        return True

def check_characters(str):
    found=False
    flag=0
    for s in str:
        if s not in allowed_chars and not s.isalnum():
            flag=1
    if flag:
        found=True
    return found

def dateformatter(delimiter,elements):
    format="%"+elements[0]+delimiter+"%"+elements[1]+delimiter+"%"+elements[2]
    return format

def format_builder():
    delimiter=[" ","-","/"]
    month=["m","b","B"]
    day=["d","D"]
    year=["y","Y"]
    elements=itertools.product(month,day,year)
    perms=[]
    for r in elements:
        l = list(itertools.permutations(r))
        perms.append(l)
    formats=[]
    for r in perms:
        products = itertools.product(r, delimiter)
        for p in products:
            formats.append(dateformatter(p[1],p[0]))
    return formats

formats=format_builder()

def validate_email(cur,email):
    flag="0"
    cleaned=email.replace(" ","")
    email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")
    if email is None:
        result=email
        flag="20"
    elif not email_regex.match(cleaned):
        result=email
        flag="21"
    elif not check_unique(cur,email):
        result=email
        flag="22"
    else:
        result=cleaned
    return result,flag

def validate_gender(prefix):
    flag="0"
    if prefix is None:
        flag="10"
    else:
        gender=prefix[0]
        if gender not in ['M','F']:
            prefix='MALE'
            flag="11"
    return prefix,flag

def formatting_phone(phone):
    cleaned=phone.replace(" ","")
    phone_regex = re.compile(r"^[\d]+$")
    flag="0"
    if phone is None:
        result=phone
        flag="30"
    elif not cleaned.isdigit():
        result=phone
        flag="31"
    else:
        first=cleaned[0]
        second=cleaned[:2]
        if(second=='62'):
            cleaned=cleaned[2:]
        if(first=='0'):
            cleaned=cleaned[1:]

        second = cleaned[:2]
        result=cleaned
        length = len(cleaned)
        if length < 8 or length > 12:
            result = phone
            flag = "32"
        elif not (cleaned[0]=='8' and second not in ['84','86']):
            result=phone
            flag="33"

    return result,flag

def formatting_telephone(telephone):
    cleaned=telephone.replace(" ","")
    ext="-"
    if cleaned=="":
        cleaned="-"
    if "|" in cleaned:
        split=cleaned.split("|")
        cleaned=split[0]
        ext=split[1]

    return cleaned,ext

def formatting_address(address):
    #address_regex=re.compile(r"^[a-zA-Z0-9/&():,.\-']*$")
    flag="0"
    address_2=None
    if not address:
        flag="50"
    if check_characters(address):
        flag="51"
    length=len(address)
    if length<5:
        flag="52"
    if length>250:
        address_2=address[250:]
    result=address
    return result,flag,address_2

def parsestring(format, s):
    formatted="null"
    for f in format:
        try:
            formatted=datetime.strptime(s, f)
        except:
            pass
        else:
            return formatted

def excludetime(d):
    ls=d.split(" ")
    for i in range(1,len(ls)):
        iftime=ls[-i]
        if ":" in iftime or "AM" in iftime:
            d=' '.join(ls[:-i])
    return d

#def fill_provid(provinsi):

def formatting_birthdate(birthdate):
    formattrue="%Y-%m-%d"
    flag="0"
    d=excludetime(birthdate)
    p=parsestring(formats,d)
    if birthdate is None:
        result=birthdate
        flag="40"
    try:
        result=p.strftime(formattrue)
    except:
        result=None
        flag="41"
    return result,flag

def save_csv(list, file, cols):
    outputdf = pd.DataFrame(list, columns=cols)
    outputdf.to_csv(file)

def to_null(d):
    if d=='0':
        return None
    else:
        return d

file='D:/data-bhinneka/membership1000.csv'
columnsOutput=['Gender','Email','Phone','Telephone','Ext','BirthDate','Address']
columnsRejected=['Gender','Email','Phone','Telephone','Ext','BirthDate','Address','Reason']
dfraw=pd.read_csv(file)
df=dfraw.fillna('0')

output=[]
output_rejected=[]

# conn_string = "host='b2c-api-dev.chzl1mgg7vpx.ap-southeast-1.rds.amazonaws.com' dbname='user_service' user='b2c_api' password='B2C4P1DEVP455W0RD'"
# conn = psycopg2.connect(conn_string)
# conn.autocommit = True
# cur = conn.cursor()

print("waktu mulai: "+str(datetime.now()))

#q='select * from public."member" order by "lastLogin" desc limit 25000'
#cur.execute(q)
#rows=cur.fetchall()
for row in df.itertuples():
    gender=validate_gender(row[5])
    email=validate_email(row[4])
    mobile=formatting_phone(str(row[6]))
    telephone=formatting_telephone(str(row[7]))
    birthdate=formatting_birthdate(str(row[9]))
    address=formatting_address(str(row[21]))
    if str(mobile[1])=="0" and str(email[1])=="0" and str(address[1])=="0":
        # cur.execute('INSERT INTO public.dc_member '
        #             '(id,"firstName","lastName",email,gender,mobile,phone,ext,"birthDate",password,salt,province,"provinceId",city,'
        #             '"cityId",district,"districtId","subDistrict","subDistrictId","zipCode",address,"jobTitle",department,"facebookId",'
        #             '"googleId","azureId","isAdmin","isStaff","signUpFrom",status,token,"lastLogin","lastBlocked",created,"lastModified",version)'
        #             ' VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);',
        #             (row[1],to_null(row[2]),to_null(row[3]),email[0],to_null(gender[0]),mobile[0],to_null(telephone[0]),to_null(telephone[1]),to_null(birthdate[0]),
        #              to_null(row[10]),to_null(row[11]),to_null(row[12]),to_null(row[13]),to_null(row[14]),to_null(row[15]),to_null(row[16]),to_null(row[17]),to_null(row[18]),
        #              to_null(row[19]),to_null(row[20]),to_null(row[21]),to_null(row[22]), to_null(row[23]), to_null(row[24]), to_null(row[25]), to_null(row[26]),
        #              to_null(row[27]), to_null(row[28]),None,to_null(row[29]), to_null(row[30]), to_null(row[31]),to_null(row[32]),to_null(row[33]),to_null(row[34]),"36"))
        output.append([gender[0],email[0],mobile[0],telephone[0],telephone[1],birthdate[0],address[0]])
    else:
        reason=""
        if(str(mobile[1])!="0"):
            reason+=error_message.get(str(mobile[1]))+", "
        if(str(email[1])!="0"):
            reason+=error_message.get(str(email[1]))+", "
        if (str(address[1]) != "0"):
            reason += error_message.get(str(address[1]))
        output_rejected.append([gender[0],email[0],mobile[0],telephone[0],telephone[1],birthdate[0],address[0],reason])
        # cur.execute('INSERT INTO public.dc_member_rejected '
        #             '(id,"firstName","lastName",email,gender,mobile,phone,ext,"birthDate",password,salt,province,"provinceId",city,'
        #             '"cityId",district,"districtId","subDistrict","subDistrictId","zipCode",address,"jobTitle",department,"facebookId",'
        #             '"googleId","azureId","isAdmin","isStaff","signUpFrom",status,token,"lastLogin","lastBlocked",created,"lastModified",version,reject_reason)'
        #             ' VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)',
        #             (row[1], to_null(row[2]), to_null(row[3]), email[0], to_null(gender[0]), mobile[0],
        #              to_null(telephone[0]), to_null(telephone[1]), to_null(birthdate[0]),
        #              to_null(row[10]), to_null(row[11]), to_null(row[12]), to_null(row[13]), to_null(row[14]),
        #              to_null(row[15]), to_null(row[16]), to_null(row[17]), to_null(row[18]),
        #              to_null(row[19]), to_null(row[20]), to_null(row[21]), to_null(row[22]), to_null(row[23]),
        #              to_null(row[24]), to_null(row[25]), to_null(row[26]),
        #              to_null(row[27]), to_null(row[28]), None, to_null(row[29]), to_null(row[30]), to_null(row[31]),
        #              to_null(row[32]), to_null(row[33]), to_null(row[34]), "36", reason))

print("waktu selesai: "+str(datetime.now()))
save_csv(output,'D:/data-bhinneka/outmembership.csv', columnsOutput)
save_csv(output_rejected, 'D:/data-bhinneka/outmembershipreject.csv', columnsRejected)