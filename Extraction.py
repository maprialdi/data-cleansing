import pandas as pd
import nltk
import re

file='C:/Users/aprialdi.pratama/Downloads/data.csv'
worksheet='data'
fields=['PropertyName','PropertyValueString']
#out='data'

df = pd.read_csv(file)
dfsel = df[fields]
df2=dfsel.to_json(orient='records')

dfaddress=df[df['PropertyName'] == 'Address.Address']

# for row in dfaddress.itertuples():
#     alamat=row[3]
#     cleaned=re.sub(r'[^\w\s]', '', alamat)
#     tokens=nltk.tokenize.word_tokenize(cleaned)
#     print(tokens)

from nltk.tag import StanfordNERTagger
import os

java_path = "C:/Program Files/Java/jdk1.8.0_171/bin/java.exe"
os.environ['JAVAHOME'] = java_path

stanford_classifier = 'D:/installer/stanford-ner-2018-02-27/stanford-ner-2018-02-27/classifiers/english.all.3class.distsim.crf.ser.gz'
stanford_ner_path = 'D:/installer/stanford-ner-2018-02-27/stanford-ner-2018-02-27/stanford-ner.jar'

st = StanfordNERTagger(stanford_classifier, stanford_ner_path, encoding='utf-8')
text = 'While in France, Christine Lagarde discussed short-term stimulus efforts in a recent interview with the Wall Street Journal.'

tokenized_text = nltk.word_tokenize(text)
classified_text = st.tag(tokenized_text)

print(classified_text)


