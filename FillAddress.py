import re
from datetime import datetime
import itertools
import pandas as pd
import psycopg2

#fungsi untuk validitas email menggunakan format standar email


def load_data(file):
    dict={}
    df = pd.read_csv(file)
    for d in df.itertuples():
        dict[d[2]] = d[3]
    return dict

dict_province=load_data("D:/data-bhinneka/AREA/AREA/bc_province.csv")
dict_city=load_data("D:/data-bhinneka/AREA/AREA/bc_city.csv")
dict_district=load_data("D:/data-bhinneka/AREA/AREA/bc_district.csv")
dict_subdistrict=load_data("D:/data-bhinneka/AREA/AREA/bc_subdistrict.csv")

def to_null(d):
    if d=='0':
        return None
    else:
        return d

output=[]
output_rejected=[]

conn_string = "host='b2c-api-dev.chzl1mgg7vpx.ap-southeast-1.rds.amazonaws.com' dbname='user_service' user='b2c_api' password='B2C4P1DEVP455W0RD'"
conn = psycopg2.connect(conn_string)
conn.autocommit = True
cur = conn.cursor()

print("waktu mulai: "+str(datetime.now()))

q='select * from public.dc_member where version=35'
cur.execute(q)
rows=cur.fetchall()
for row in rows:
    provinceId=dict_province.get(row[11],None)
    cityId = dict_city.get(row[13],None)
    districtId=dict_district.get(row[15],None)
    subdistrictId=dict_subdistrict.get(row[17],None)
    usrid=row[1]
    cur.execute('UPDATE public.dc_member SET "provinceId"=%s, "cityId"=%s, "districtId"=%s, "subDistrictId"=%s where id=%s and version=35',(provinceId,cityId,districtId,subdistrictId,usrid))

print("waktu selesai: "+str(datetime.now()))
#save_csv(output,'D:/data-bhinneka/outmembership.csv', columnsOutput)
#save_csv(output_rejected, 'D:/data-bhinneka/outmembershipreject.csv', columnsRejected)