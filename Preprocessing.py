import re

email_regex = re.compile(r"[^@]+@[^@]+\.[^@]+")
phone_regex = re.compile(r"(?:\+62)?0?8\d{2}(\d{8})")

test_email="mohammad.aprialdi@gmail.com"
test_phone="+6281231843784"

domain = re.search("@[\w.]+", test_email)
print(domain)

if not email_regex.match(test_email):
    print("gagal")
else:
    print("sukses")

if not phone_regex.match(test_phone):
    print("gagal")
else:
    print("sukses")