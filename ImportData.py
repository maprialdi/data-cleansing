#kode untuk convert data kodepos berformat xlsx ke dataframe berformat json

import pandas as pd
import json

file='D:/data-bhinneka/AREA/AREA/bc_province.csv'
worksheet='bc_province'
out='data.json'

def create_dict(df, level):
    dict={}
    for row in df.itertuples():
        data = row[1]
        dict[data] = level
    return dict

def getdata(level):
    fields = [level]
    xls_file = pd.ExcelFile(file)
    dfall = xls_file.parse(worksheet)
    df=dfall[fields]
    return df

dict_data={}

#fields=['No Kode Pos', 'Kelurahan','Kecamatan','Kabupaten','Propinsi']
#for field in fields:
df=pd.read_csv(file)
for d in df.itertuples():
    dict_data[d[2]]=d[3]
print(dict_data)
#dict_data.update(create_dict(df, field))

#with open(out, 'w') as fp:
    #json.dump(dict_data, fp)